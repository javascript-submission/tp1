import {setState} from "../store"
import list from "./list";

/* FIXME:
*
* export a function that adds a new element to the store.
*
* Rules:
* - add must be able to take either a single element
* or an array of new elements
* - you must use the functions from "../store"
*
*/

const add = (url) => {
    const lst = list();
    lst.push(url);
    setState(lst);
};

export default add;
