/* FIXME:
*
* export a function that updates a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
* - the updated element must not share the same reference as the previous one.
*
*/

import {setState} from "../store";
import list from "./list";

const update = (old, elt) => {
    const lst = list();
    if (lst.indexOf(old) > -1)
        lst[lst.indexOf(old)] = elt;
    setState(lst);

};

export default update;
