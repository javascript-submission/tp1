import list from "./list";
import {setState} from "../store";

/* FIXME:
*
* export a function that removes a single element from the store.
*
* Rules:
* - you must use the functions from "../store"
*
*/

const remove = (uri) => {
    const lst = list();
    console.log("Removing " + uri + " [" + lst.indexOf(uri) + "]");
    lst.splice(lst.indexOf(uri), 1);
    setState(lst);
};

export default remove;
